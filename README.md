# CRUD API using Express and Typescript

## Installation

1. Clone the repository
```
git clone https://gitlab.com/amalrajan/crud-api-express-ts.git
```

2. Start Redis
```
docker run redis:latest -p 6379:637
```


## Sample requests

1. Get all posts
```
curl --location 'http://localhost:6060/posts'
```

2. Get post by Id
```
curl --location 'http://localhost:6060/posts/362241'
```

3. Delete a post by Id
```
curl --location --request DELETE 'http://localhost:6060/posts/247232'
```

4. Add new post

```
curl --location 'http://localhost:6060/posts' \
--header 'Content-Type: application/json' \
--data '{
    "userId": 1,
    "title": "Title 1",
    "body": "Body 2"
}'
```

5. Update post by Id
```
curl --location --request PUT 'http://localhost:6060/posts/247232' \
--header 'Content-Type: application/json' \
--data '{
    "userId": 23,
    "title": "Modified",
    "body": "Body 4"
}'
```