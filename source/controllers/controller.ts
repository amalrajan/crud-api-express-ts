import { Request, Response } from "express";
import PostService from "../service/service";

// Get all posts
const getPosts = async (req: Request, res: Response): Promise<void> => {
    try {
        const posts = await PostService.readAllFromRedis();
        console.log(posts);
        res.status(200).json(posts);
    } catch (error) {
        console.error("Error retrieving posts:", error);
        res.status(500).send("Internal Server Error");
    }
};

// Get a post by ID
const getPostById = async (req: Request, res: Response): Promise<void> => {
    try {
        const { id } = req.params;
        const post = await PostService.readFromRedis(Number(id));
        if (post) {
            res.status(200).json(post);
        } else {
            res.status(404).send("Post not found");
        }
    } catch (error) {
        console.error("Error retrieving post:", error);
        res.status(500).send("Internal Server Error");
    }
};

// Add a new post
const addPost = async (req: Request, res: Response): Promise<void> => {
    try {
        const { userId, title, body } = req.body;
        const id = PostService.getUniqueIDFromString(title);
        const post = { userId, id, title, body };

        await PostService.writeToRedis(post);
        res.status(201).send("Post added successfully");
    } catch (error) {
        console.error("Error adding post:", error);
        res.status(500).send("Internal Server Error");
    }
};

// Delete a post
const deletePost = async (req: Request, res: Response): Promise<void> => {
    try {
        const { id } = req.params;
        await PostService.deleteFromRedis(Number(id));
        res.status(200).send("Post deleted successfully");
    } catch (error) {
        console.error("Error deleting post:", error);
        res.status(500).send("Internal Server Error");
    }
};

// Update a post
const updatePost = async (req: Request, res: Response): Promise<void> => {
    try {
        const { id } = req.params;
        const { userId, title, body } = req.body;
        const post = { userId, id: Number(id), title, body };

        await PostService.updateInRedis(post);
        res.status(200).send("Post updated successfully");
    } catch (error) {
        console.error("Error updating post:", error);
        res.status(500).send("Internal Server Error");
    }
};

export default {
    getPosts,
    getPostById,
    addPost,
    deletePost,
    updatePost,
};
