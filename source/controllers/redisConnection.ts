import Redis from "ioredis";


class RedisConnection {
    private client: Redis;

    constructor() {
        this.client = new Redis();

        // Handle connection events
        this.client.on("connect", () => {
            console.log("Connected to Redis");
        });

        this.client.on("error", (error) => {
            console.error("Redis error:", error);
        });
    }

    public async get(key: string): Promise<string | null> {
        return this.client.get(key);
    }

    public async set(key: string, value: string): Promise<string> {
        return this.client.set(key, value);
    }

    public async del(key: string): Promise<number> {
        return this.client.del(key);
    }

    public async keys(pattern: string): Promise<string[]> {
        return this.client.keys(pattern);
    }

    public async mget(keys: string[]): Promise<string[]> {
        const values = await this.client.mget(keys);
        return values.map((value) => (value !== null ? value : ""));
    }
}

export default RedisConnection;
