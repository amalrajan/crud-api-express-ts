import express, { Express } from "express";
import routes from "./routes/posts";

const app: Express = express();
const port: number = 6060;

app.use(express.json());
app.use(routes);

app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});
