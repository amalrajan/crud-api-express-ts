import { createHash } from "crypto";
import RedisConnection from "../controllers/redisConnection";

interface Post {
    userId: number;
    id: number;
    title: string;
    body: string;
}

const redisConnection = new RedisConnection();

function getUniqueIDFromString(input: string): number {
    const hash = createHash("sha256");
    const hashedData = hash.update(input).digest("hex");
    const numericID = parseInt(hashedData, 16);
    const sixDigitID = numericID % 1000000; // Extract last 6 digits
    return sixDigitID;
}

async function writeToRedis(post: Post): Promise<void> {
    try {
        const key = `post:${post.id}`; // Generate a unique key for the post
        const value = JSON.stringify(post); // Convert the post object to a JSON string

        await redisConnection.set(key, value); // Set the value in Redis
        console.log("Post written to Redis successfully");
    } catch (error) {
        console.error("Error writing post to Redis:", error);
    }
}

async function readFromRedis(postId: number): Promise<Post | null> {
    try {
        const key = `post:${postId}`; // Generate a unique key for the post
        const postString = await redisConnection.get(key); // Get the value of the key

        if (postString) {
            const post = JSON.parse(postString);
            console.log("Post retrieved from Redis successfully");
            return post;
        } else {
            console.log("Post not found in Redis");
            return null;
        }
    } catch (error) {
        console.error("Error retrieving post from Redis:", error);
        return null;
    }
}

async function readAllFromRedis(): Promise<Post[]> {
    try {
        const keys = await redisConnection.keys("post:*"); // Get all keys starting with "post:"
        const postStrings = await redisConnection.mget(keys); // Get the values of all the keys

        const posts = [];
        for (const postString of postStrings) {
            if (postString) {
                const post = JSON.parse(postString);
                posts.push(post);
            }
        }

        console.log("Posts retrieved from Redis successfully");
        return posts;
    } catch (error) {
        console.error("Error retrieving posts from Redis:", error);
        return [];
    }
}

async function deleteFromRedis(postId: number): Promise<void> {
    try {
        const key = `post:${postId}`; // Generate a unique key for the post
        await redisConnection.del(key); // Delete the value from Redis
        console.log("Post deleted from Redis successfully");
    } catch (error) {
        console.error("Error deleting post from Redis:", error);
    }
}

async function updateInRedis(post: Post): Promise<void> {
    try {
        const key = `post:${post.id}`; // Generate a unique key for the post
        const value = JSON.stringify(post); // Convert the post object to a JSON string

        await redisConnection.set(key, value); // Set the value in Redis
        console.log("Post updated in Redis successfully");
    } catch (error) {
        console.error("Error updating post in Redis:", error);
    }
}

export default {
    writeToRedis,
    readFromRedis,
    readAllFromRedis,
    deleteFromRedis,
    updateInRedis,
    getUniqueIDFromString,
};
